//
//  WidgetQuotesEntryView.swift
//  WidgetQuotesExtension
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import WidgetKit
import SwiftUI

struct WidgetQuotesEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        ZStack{
            Rectangle().fill(Color.black)
            VStack{
                Spacer()
                Text("\"\(self.entry.quote)\"")
                    .multilineTextAlignment(.center)
                    .quoteFont(size: 24, scale: 0.5)
                    .padding()
                Spacer()
                HStack{
                    Spacer()
                    Text(" - \(self.entry.author)")
                        .font(.caption)
                        .italic()
                        .padding()
                }
            }.foregroundColor(.white)
        }
    }
}
