//
//  WidgetQuotes.swift
//  WidgetQuotes
//
//  Created by Ausianovich Kanstantsin on 08.10.2020.
//

import WidgetKit
import SwiftUI

@main
struct WidgetQuotes: Widget {
    let kind: String = "WidgetQuotes"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            WidgetQuotesEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
        .supportedFamilies([.systemMedium])
    }
}

struct WidgetQuotes_Previews: PreviewProvider {
    static var previews: some View {
        WidgetQuotesEntryView(entry: WidgetEntry(date: Date(), quote: "This Too Shall Pass...", author: "Solomon"))
            .previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}
