//
//  WidgetEntry.swift
//  WidgetQuotesExtension
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import WidgetKit

struct WidgetEntry: TimelineEntry {
    var date: Date
    let quote: String
    let author: String
}
