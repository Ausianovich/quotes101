//
//  Provider.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import WidgetKit
import SwiftUI

struct Provider: TimelineProvider {
    private var jsonDecoder = JSONDecoder()
    
    func placeholder(in context: Context) -> WidgetEntry {
        self.createWidgetPlaceHolder()
    }

    func getSnapshot(in context: Context, completion: @escaping (WidgetEntry) -> ()) {
        let entry = self.createWidgetPlaceHolder()
        completion(entry)
    }

    private func createWidgetPlaceHolder() -> WidgetEntry {
        return WidgetEntry(date: Date(), quote: "Everything goes by, this too shall pass.", author: "King Solomon")
    }
    
    func getTimeline(in context: Context, completion: @escaping (Timeline<WidgetEntry>) -> ()) {
        let timeline = Timeline(entries: self.getEntries(), policy: .atEnd)
        completion(timeline)
    }
    
    private func getEntries() -> [WidgetEntry] {
        guard let jsonQuotesURL = self.getJSONQuotesURL() else {return []}
        let data = self.getData(from: jsonQuotesURL)
        let widgetEntry: [WidgetEntry] = self.parceJSONData(from: data)
        
        return widgetEntry
    }
    
    private func getJSONQuotesURL() -> URL? {
        return FileManager.default
                          .containerURL(forSecurityApplicationGroupIdentifier: "group.quotes.contents")?
                          .appendingPathComponent("JSONQuotes.json")
    }
    
    private func getData(from url: URL) -> Data {
        var data = Data()
        do {
            data = try Data(contentsOf: url)
        } catch {
            print(error.localizedDescription)
        }
        
        return data
    }
    
    private func parceJSONData(from data: Data) -> [WidgetEntry] {
        let quotes = self.getQuotes(form: data)
        let widgetEntries = self.createWidgetEntries(from: quotes)
        
        return widgetEntries
    }
    
    private func getQuotes(form data: Data) -> [QuoteModel] {
        var quotes: [QuoteModel] = []
        do {
            quotes = try self.jsonDecoder.decode(QuotesModel.self, from: data).quotes.shuffled()
        } catch {
            print(error.localizedDescription)
        }
        return quotes
    }
    
    private func createWidgetEntries (from array: [QuoteModel]) -> [WidgetEntry] {
        var widgetEntries: [WidgetEntry] = []
        
        for index in 0 ..< array.count {
            let entryDate = self.createEntryDate(by: index)
            let widgetEntry = WidgetEntry(date: entryDate, quote: array[index].quote, author: array[index].author)
            widgetEntries.append(widgetEntry)
        }
        
        return widgetEntries
    }
    
    private func createEntryDate(by index: Int) -> Date {
        guard let entryDate = Calendar.current.date(byAdding: .hour, value: index, to: Date()) else {return Date()}
        
        return entryDate
    }
}
