//
//  _01QoutesApp.swift
//  101Qoutes
//
//  Created by Ausianovich Kanstantsin on 08.10.2020.
//

import SwiftUI

@main
struct QoutesApp: App {
    private var quotes = Quotes()
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(self.quotes)
        }
    }
}
