//
//  Quotes.swift
//  101Qoutes
//
//  Created by Ausianovich Kanstantsin on 08.10.2020.
//

import Foundation

import Foundation

// MARK: - Quotes
struct QuotesModel: Codable {
    let quotes: [QuoteModel]
}

// MARK: - Quote
struct QuoteModel: Codable, Equatable {
    let number, quote, author: String
}
