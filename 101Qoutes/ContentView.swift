//
//  ContentView.swift
//  101Qoutes
//
//  Created by Ausianovich Kanstantsin on 08.10.2020.
//

import SwiftUI

final class Quotes: ObservableObject {
    private var quotes: [QuoteModel] = []
    private var jsonDecoder = JSONDecoder()
    
    @Published var currentQuote: QuoteModel?
    
    init() {
        self.loadQuotes()
        self.currentQuote = self.quotes.randomElement()
    }
    
    func getRandomQuote() {
        let newQuote = self.quotes.randomElement()
        guard let currentQuote = self.currentQuote else {return}
        
        if currentQuote == newQuote {
            self.getRandomQuote()
        } else {
            self.currentQuote = newQuote
        }
    }
    
    private func loadQuotes() {
        guard let mainURL = Bundle.main.url(forResource: "JSONQuotes", withExtension: "json") else {return}
        do {
            let jsonData = try Data(contentsOf: mainURL)
            self.quotes = try self.jsonDecoder.decode(QuotesModel.self, from: jsonData).quotes
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

struct ContentView: View {
    
    @EnvironmentObject var quotes: Quotes
    
    var body: some View {
        ZStack {
            Rectangle().fill(Color.black).ignoresSafeArea(.all)
            VStack{
                Spacer()
                Text("\(self.quotes.currentQuote?.number ?? "") \"\(self.quotes.currentQuote?.quote ?? "")\"")
                    .font(.title)
                    .italic()
                    .padding(20)
                HStack{
                    Spacer()
                    Text("- \(self.quotes.currentQuote?.author ?? "")")
                        .font(.title3)
                        .italic()
                        .padding()
                }
                Spacer()
                Button("One more...") {
                    self.quotes.getRandomQuote()
                }
            }.foregroundColor(.white)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
