//
//  _01QuotesApp.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 08.10.2020.
//

import SwiftUI

@main
struct QuotesApp: App {
    
    private var quotes = Quotes()
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(self.quotes)
                .environment(\.isAppAlreadyLaunchedOnce, self.isAppAlreadyLaunchedOnce())
        }
    }
    
    private func isAppAlreadyLaunchedOnce() -> Bool {
        let defaults = UserDefaults.standard
        
        if let _ = defaults.string(forKey: "isAppAlreadyLaunchedOnce") {
            return true
        } else {
            defaults.setValue(true, forKey: "isAppAlreadyLaunchedOnce")
            return false
        }
    }
}
