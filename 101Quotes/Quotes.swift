//
//  Quotes.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import SwiftUI
import WidgetKit

final class Quotes: ObservableObject {
    private var quotes: [QuoteModel] = []
    private var jsonDecoder = JSONDecoder()
    
    @Published var currentQuote: QuoteModel?
    @Environment(\.isAppAlreadyLaunchedOnce) var isAppAlreadyLaunchedOnce
    
    init() {
        self.loadQuotes()
        self.setRandomCurrentQuote()
        self.writeSharedFileInAppGroupFolder()
        WidgetCenter.shared.reloadAllTimelines()
    }
    
    private func loadQuotes() {
        guard let jsonQuotesURL = self.getJSONQuotesURL() else {return}
        let data = self.getData(contentsOf: jsonQuotesURL)
        self.parseToQuotes(from: data)
    }
    
    func setRandomCurrentQuote() {
        guard let randomQuote = self.quotes.randomElement() else {return}
        if self.currentQuoteIsEqual(to: randomQuote) {
            self.setRandomCurrentQuote()
        } else {
            self.currentQuote = randomQuote
        }
    }
    
    private func writeSharedFileInAppGroupFolder() {
        if !self.isAppAlreadyLaunchedOnce {
            guard let jsonQuotesURL = self.getJSONQuotesURL(),
                  let appGroupFileURL = self.getAppGroupFileURL() else {return}

            let quotesData = self.getData(contentsOf: jsonQuotesURL)
            self.write(quotesData, to: appGroupFileURL)
        }
    }
    
    private func getJSONQuotesURL() -> URL? {
        return Bundle.main.url(forResource: "JSONQuotes", withExtension: "json")
    }
    
    private func getData(contentsOf url: URL) -> Data {
        var data = Data()
        do {
            data = try Data(contentsOf: url)
        } catch {
            print(error.localizedDescription)
        }
        return data
    }
    
    private func parseToQuotes(from data: Data){
        do {
            self.quotes = try self.jsonDecoder.decode(QuotesModel.self, from: data).quotes
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func currentQuoteIsEqual(to element: QuoteModel) -> Bool {
        guard let currentElement = self.currentQuote else {return false}
        return currentElement == element
    }
    
    private func getAppGroupFileURL() -> URL? {
        guard let appGroupURL = self.getAppGroupURL() else {return nil}
        return appGroupURL.appendingPathComponent("JSONQuotes.json")
    }
    
    private func getAppGroupURL() -> URL? {
        return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.quotes.contents")
    }
    
    private func write(_ data: Data, to url: URL) {
        do {
            try data.write(to: url)
        } catch {
            print(error.localizedDescription)
        }
    }
}
