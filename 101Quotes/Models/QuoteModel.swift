//
//  QuoteModel.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import Foundation

struct QuoteModel: Codable, Equatable {
    let number, quote, author: String
}
