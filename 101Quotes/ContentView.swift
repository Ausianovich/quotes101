//
//  ContentView.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 08.10.2020.
//

import SwiftUI
import WidgetKit

struct ContentView: View {
    
    @EnvironmentObject var quotes: Quotes
    
    var body: some View {
        ZStack {
            Rectangle().fill(Color.black).ignoresSafeArea(.all)
            VStack{
                Spacer()
                Text("\(self.quotes.currentQuote?.number ?? "") \"\(self.quotes.currentQuote?.quote ?? "")\"")
                    .quoteFont(size: 24, scale: 0.8)
                    .padding(20)
                HStack{
                    Spacer()
                    Text("- \(self.quotes.currentQuote?.author ?? "")")
                        .quoteFont(size: 18)
                        .padding()
                }
                Spacer()
                Button {
                    self.quotes.setRandomCurrentQuote()
                } label: {
                    Text("One more...")
                        .quoteFont(size: 18)
                        .padding()
                }
            }.foregroundColor(.white)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
