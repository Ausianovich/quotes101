//
//  QuoteFont.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import SwiftUI

struct QuoteFont: ViewModifier {
    var size: CGFloat
    var scale: CGFloat
    func body(content: Content) -> some View {
        content
            .font(.custom("Georgia", size: self.size))
            .minimumScaleFactor(self.scale)
    }
}
