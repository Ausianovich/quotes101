//
//  AppAlreadyLaunchedOnce.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import SwiftUI

struct AppAlreadyLaunchedOnce: EnvironmentKey {
    static var defaultValue: Bool = false
}
