//
//  View + Extention.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import SwiftUI

extension View {
    func quoteFont(size: CGFloat, scale scaleFactor: CGFloat = 1) -> some View {
        self.modifier(QuoteFont(size: size, scale: scaleFactor))
    }
}
