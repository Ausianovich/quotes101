//
//  EnvironmentValues.swift
//  101Quotes
//
//  Created by Ausianovich Kanstantsin on 11.10.2020.
//

import SwiftUI

extension EnvironmentValues {
    var isAppAlreadyLaunchedOnce: Bool {
        get {
            return self[AppAlreadyLaunchedOnce]
        }
        set {
            self[AppAlreadyLaunchedOnce] = newValue
        }
    }
}
